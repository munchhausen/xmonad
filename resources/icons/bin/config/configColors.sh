#!/usr/bin/env bash

# Edit this file to use your 16-color configuration. The colors are chosen in the style of a
# 16-color terminal.

# black
color0="#0b0b0f"
color8="#7970a9"

# red
color1="#ff5555"
color9="#ff6e6e"

# green
color2="#8aff80"
colorA="#a2ff99"

# yellow
color3="#ffff80"
colorB="#ffff99"

# blue
color4="#9580ff"
colorC="#aa99ff"

# magenta
color5="#ff80bf"
colorD="#ff99cc"

# cyan
color6="#80ffea"
colorE="#99ffee"

# white
color7="#f8f8f2"
colorF="#454158"
