module EXmobar.Helpers where


import Theme ( BarTheme (..) )


wrap :: String -> String -> String -> String
wrap left right middle = left ++ middle ++ right

pad :: String -> String
pad = wrap " " " "

action :: String -> String -> String -> String
action act button = wrap ("<action=`" ++ act ++ "` button=" ++ button ++ ">") "</action>"

icon :: String -> String
icon p = "<icon=" ++ p ++ "/>"

xpm :: String -> String
xpm p = icon $ p ++ ".xpm"

xbm :: String -> String
xbm p = icon $ p ++ ".xbm"


lnh :: BarTheme -> [String]
lnh t = [ "--low", g
        , "--normal", y
        , "--high", r
        ]
            where (g,y,r) = lnhCol t

lnh' :: BarTheme -> [String]
lnh' t = [ "--high", g
         , "--normal", y
         , "--low", r
         ]
             where (g,y,r) = lnhCol t
