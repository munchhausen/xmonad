module EXmobar.Config ( myConfig ) where

import Data.Default
import Data.List
import System.Directory
import System.FilePath
import Xmobar

import Env
import EXmobar.Helpers
import EXmobar.Monitors
import Theme ( BarTheme (..) )



baseConfig :: BarTheme -> FilePath -> Config
baseConfig t p = defaultConfig
    { font = barFont t
    , additionalFonts = barExtraFonts t
    , bgColor = barBG t
    , fgColor = barFG t
    , position = Top
    , lowerOnStart = True
    , hideOnStart = False
    , persistent = True
    , border = NoBorder
    , borderWidth = 0
    , iconRoot = p </> "icons"
    , sepChar = "%"
    , alignSep = "}{"
    }

sep :: String
sep = pad $ xpm "separators/separator"

menu :: String
menu = pad . action "jgmenu_run" "1" $ xpm "menu/menu"

hasBattery :: IO Bool
hasBattery = do
    let sysDir = "/sys/class/power_supply"
    batteries <- listDirectory sysDir
    return $ case batteries of
             [] -> False
             otherwise -> True


myConfig :: IO Config
myConfig = do
    let theme = def :: BarTheme
    path <- resourceHome
    bat <- hasBattery
    return $ (baseConfig theme path)
        { commands = [ Run $ weather path
                     , Run $ mail path
                     , Run $ trayPadding path
                     , Run date
                     , Run $ multiCPU theme
                     , Run $ memory theme
                     , Run $ battery theme
                     , Run $ dynNetwork theme
                     , Run alsa
                     , Run UnsafeStdinReader
                     ]
        , template = menu ++ "%UnsafeStdinReader% }{" ++ sep
                  ++ (intercalate sep $ wrap "%" "%" <$> modules bat)
        }
            where
                modules b =
                    [ "multicpu"
                    , "memory"
                    , "dynnetwork"
                    , "weather"
                    , "mail"
                    , "alsa:default:Master"
                    ]
                 ++ ( if b then ["battery"] else [] )
                 ++ [ "date"
                    , "traypadding"
                    ]



