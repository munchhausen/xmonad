module EXmobar.Monitors where

import Xmobar
import System.FilePath

import Theme ( BarTheme )
import EXmobar.Helpers

-- scriptRoot :: FilePath -> FilePath
-- scriptRoot p = p </> "bin"

script :: FilePath -> FilePath -> FilePath
script p f = p </> "bin" </> f


weather :: FilePath -> Command
weather p = Com (script p "weather.sh") [] "weather" 1000

mail :: FilePath -> Command
mail p = Com (script p "mail.sh") [] "mail" 100

trayPadding :: FilePath -> Command
trayPadding p = Com (script p "traypadding.sh") ["stalonetray"] "traypadding" 30

date :: Date
date = Date ( (action "term -e ikhal" "1" $ xpm "calendar/%d") ++ " %I:%M%P" ) "date" 10

multiCPU :: BarTheme -> Monitors
multiCPU t = MultiCpu (
    [ "--template", (action "term -e htop" "1" "<ipat>") ++ " <total>%"
    , "--Low", "25"
    , "--High", "75"
    ]
    ++ (lnh t) ++
    [ "--"
    , "--load-icon-pattern", xpm "cpu/cpu_%%"
    ] ) 20

memory :: BarTheme -> Monitors
memory t = Memory (
    [ "--template", (action "term -e htop" "1" "<usedipat>") ++ " <usedratio>%"
    , "--Low", "25"
    , "--High", "75"
    ]
    ++ (lnh t) ++
    [ "--"
    , "--used-icon-pattern", xpm "ram/ram_%%"
    ] ) 20

battery :: BarTheme -> Monitors
battery t = Battery (
    [ "--template", (action "xfce4-power-manager -c" "1" "<leftipat>") ++ " <acstatus>"
    , "--Low"      , "20"
    , "--High"     , "80"
    ]
    ++ (lnh' t) ++
    [ "--"
    , "--on-icon-pattern"   , xpm "battery/on/battery_on_%%"
    , "--off-icon-pattern"  , xpm "battery/off/battery_off_%%"
    , "--idle-icon-pattern" , xpm "battery/idle/battery_idle_%%"
    , "-o" , "<left>% (<timeleft>)"
    , "-O" , "<left>% C"
    , "-i" , "I"
    ] ) 50

dynNetwork :: BarTheme -> Monitors
dynNetwork t = DynNetwork (
    [ "--template"  , "<tx>" ++ (action "nm-connection-editor" "1" "<txipat><rxipat>") ++ "<rx>"
    , "--Low"       , "100000"   -- units: B/s
    , "--High"      , "1000000"  -- units: B/s
    , "--maxtwidth" , "0"
    ]
    ++ (lnh t) ++
    [ "--"
    , "--rx-icon-pattern" , xpm "network/rx/network_rx_%%"
    , "--tx-icon-pattern" , xpm "network/tx/network_tx_%%"
    ] ) 20

alsa :: Monitors
alsa = Alsa "default" "Master"
    [ "--template", "<status><volumebar><volume>%" ]
