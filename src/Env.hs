module Env ( eXMonadHome
           , resourceHome
           ) where

import Data.Maybe
import System.Directory
import System.Environment
import System.FilePath

eXMonadHome :: IO FilePath
eXMonadHome = lookupEnv "XMonad_HOME" >>= maybe ((</> ".config/xmonad") <$> getHomeDirectory) (return . id)

resourceHome :: IO FilePath
resourceHome = (</> "resources") <$> eXMonadHome
