module EXMonad.Config.Workspaces where


import XMonad.Actions.DynamicProjects
import XMonad.Util.Run

import EXMonad.Config.Apps


ws1 = "main"
ws2 = "www"
ws3 = "TeX"
ws4 = "code"
ws5 = "aux"
ws6 = "chat"
ws7 = "media"
ws8 = "mail"
ws9 = "sys"

myWorkspaces :: [String]
myWorkspaces = [ws1, ws2, ws3, ws4, ws5, ws6, ws7, ws8, ws9]


altLayoutWS :: [String]
altLayoutWS = ["TeX", "code", "mail"]



projects :: [Project]
projects = [ Project { projectName = "mail"
                     , projectDirectory = "~/"
                     , projectStartHook = Just $ do
                         safeSpawn term ["-e", "mutt"]
                         safeSpawn term ["-e", "ikhal"]
                     }
           ]
