module EXMonad.Config.Theme ( myTabTheme
                            , myPromptTheme
                            ) where

import Theme ( WMTheme (..)
             , WinTheme (..)
             , PromptTheme (..)
             )

import Data.Default

import XMonad.Layout.Tabbed
import XMonad.Prompt


tabTheme :: WMTheme -> Theme
tabTheme t = def
    { fontName = wmFont t
    , activeColor = winBG $ activeWin t
    , inactiveColor = winBG $ inactiveWin t
    , activeBorderColor = winBorder $ activeWin t
    , inactiveBorderColor = winBorder $ inactiveWin t
    , activeTextColor = winFG $ activeWin t
    , inactiveTextColor = winFG $ inactiveWin t
    }

myTabTheme :: Theme
myTabTheme = tabTheme def


promptTheme :: PromptTheme -> XPConfig
promptTheme t = def
    { font = pFont t
    , bgColor = pBG t
    , fgColor = pFG t
    , fgHLight = pHLFG t
    , bgHLight = pHLBG t
    , borderColor = pBorder t
    , promptBorderWidth = 1
    , position = Top
    , alwaysHighlight = True
    }

myPromptTheme :: XPConfig
myPromptTheme = promptTheme def
