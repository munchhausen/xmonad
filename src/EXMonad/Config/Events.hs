module EXMonad.Config.Events ( myManageHook
                             , myHandleEventHook
                             ) where

import EXMonad.Config.Commands

import XMonad

import XMonad.Actions.SpawnOn

import XMonad.Hooks.InsertPosition
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ServerMode

import XMonad.Layout.Fullscreen


myManageHook :: ManageHook
myManageHook = manageSpecific
           <+> manageOneSpecific
           <+> manageDocks
           <+> fullscreenManageHook
           <+> manageSpawn
               where
                   manageSpecific = composeAll . concat $
                       -- [ [ isDialog <&&> className =? w --> doCenterFloat | w <- myWWW ]
                       [ [ className =? w --> doShift "www"   | w <- myWWW ]
                       , [ className =? c --> doShift "chat"  | c <- myChat ]
                       , [ className =? m --> doShift "media" | m <- myMedia ]
                       , [ className =? f --> doCenterFloat   | f <- myFloats ]
                       ]
                   manageOneSpecific = composeOne
                       [ transience
                       , resource =? "desktop_window"         -?> doIgnore
                       , isWinRole =? "GtkFileChooserDialog"  -?> doCenterFloat
                       , isWinRole =? "pop-up"                -?> doCenterFloat
                       , isSkipTask <&&> className =? "Skype" -?> insertPosition Below Older -- prevent skype popup window from taking focus
                       , isSplash                             -?> doCenterFloat
                       , isDialog                             -?> doCenterFloat
                       , isFullscreen                         -?> doFullFloat
                       , pure True                            -?> insertPosition Below Newer -- place new windows right below the current one and focus it
                       ]
                   isSplash = isInProperty "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_SPLASH"
                   isSkipTask = isInProperty "_NET_WM_STATE" "_NET_WM_STATE_SKIP_TASKBAR"
                   isWinRole  = stringProperty "WM_WINDOW_ROLE"
                   myWWW = [ "browser", "qutebrowser", "Firefox", "Vivaldi-stable", "brave-browser" ]
                   myChat = [ "Skype", "zoom" ]
                   myMedia = [ "vlc", "mpv", "Kodi" ]
                   myFloats = [ "Pavucontrol", "Wicd-client.py" ]



-- myHandleEventHook :: Event -> X Data.Semigroup.Internal.All
myHandleEventHook = docksEventHook
                <+> serverModeEventHookCmd' myCommands -- for xmonadctl
                <+> handleEventHook def
                <+> XMonad.Layout.Fullscreen.fullscreenEventHook -- constrain fullscreen to a window

