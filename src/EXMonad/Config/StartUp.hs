module EXMonad.Config.StartUp ( myStartupHook ) where

import XMonad
import XMonad.Hooks.SetWMName
import XMonad.Util.Cursor
import XMonad.Util.SpawnOnce


myStartupHook :: X ()
myStartupHook = do
    setWMName "LG3D"
    setDefaultCursor xC_left_ptr
    spawnOnce "polkit.start"
    spawnOnce "power-manager.start"
    spawnOnce "network-manager.start"
    spawnOnce "disk-mount.start"
    spawnOnce "bluetooth.start"
    spawnOnce "notifications.start"
    spawnOnce "tray.start"
    spawnOnce "locker.start"
    spawnOnce "bg.start"
    spawnOnce "sxhkd.start"
