module EXMonad.Config.Apps where

term = "term"
altTerm = "alt_term"
launcher = "dmenu-run"
browser = "browser"
altBrowser = "alt_browser"

calendar_tui = "ikhal"
mail_tui = "mutt"
