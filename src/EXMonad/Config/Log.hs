module EXMonad.Config.Log ( myLogHook ) where

import Theme ( BarTheme (..), WSTheme (..) )

import Control.Monad (forM_)
import Data.Default
import System.IO

import XMonad
import XMonad.Actions.CopyWindow
import qualified XMonad.StackSet as W
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops




myLogHook :: [Handle] -> X ()
myLogHook hs = do
    copies <- wsContainingCopies
    wCount <- windowCount
    let wrapIt' wins opt ws = wrapIt (ws `elem` copies) wins opt ws
    ewmhDesktopsLogHook
    dynamicLogWithPP $ def
        { ppTitle  = xmobarColor (titleFG t) (titleBG t) . pad . shorten 80 . xmobarStrip
        , ppCurrent = wrapIt' (wCount > 0) (currentWS t)
        , ppVisible = wrapIt' True (visibleWS t)
        , ppVisibleNoWindows = Just $ wrapIt' False (visibleWS t)
        , ppHidden = wrapIt' True (hiddenWS t)
        , ppHiddenNoWindows = wrapIt' False (hiddenWS t)
        , ppUrgent = wrapIt' True (urgentWS t)
        , ppSep    = " "
        , ppWsSep  = "<icon=separators/wsseparator.xpm/>"
        , ppLayout = wrap "<icon=layouts/" ".xpm/>"
        , ppOrder  = \(ws:l:t:ex) -> ws : l : ex ++ [t]
        , ppOutput = \str -> forM_ hs (flip hPutStrLn str)
        , ppExtras = (Just . show <$> windowCount) : []
        }
            where
                t = def :: BarTheme
                windowCount = gets $ length . W.integrate' . W.stack . W.workspace . W.current . windowset



clickable :: WorkspaceId -> String -> String
clickable w = xmobarAction ("xmonadctl view\\\"" ++ w ++ "\\\"") "1"

colorize :: WSTheme -> String -> String
colorize opt = xmobarColor (wsFG opt) (wsBG opt)

wrapIt :: Bool -> Bool -> WSTheme -> WorkspaceId -> String
wrapIt copies windows opt ws = clickable ws . colorize opt $ (wsWrap opt) windows copies ws
