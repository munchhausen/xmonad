module EXMonad.Config.Keys ( myModMask
                           , myKeys
                           , myExtraMouseKeys
                           ) where

import System.Exit


import Theme ( BarTheme (..) )

import EXMonad.Actions.ConditionalKeys
import EXMonad.Actions.SelectWindow

import EXMonad.Config.Apps
import EXMonad.Config.Commands
import EXMonad.Config.Extras
import EXMonad.Config.Layouts ( layoutList )
import EXMonad.Config.Theme ( myPromptTheme )
import EXMonad.Config.Workspaces ( myWorkspaces )

import EXMonad.Util.Dmenu ( dmenuRun )



import XMonad hiding ( (|||) )
import qualified XMonad.StackSet as W

import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleSelectedLayouts
import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.FocusNth
import XMonad.Actions.Navigation2D
import XMonad.Actions.WindowBringer
import XMonad.Actions.WithAll

import XMonad.Hooks.ManageDocks

import XMonad.Layout.LayoutCombinators
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.ResizableTile
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation

import XMonad.Util.EZConfig
import XMonad.Util.Run (safeSpawn)

import XMonad.Prompt
import XMonad.Prompt.ConfirmPrompt



myModMask :: ButtonMask
myModMask = mod4Mask


myKeys, appKeys, winKeys, navKeys, wsKeys, loKeys, ctrlKeys :: [(String, X ())]
myKeys = appKeys ++ winKeys ++ loKeys ++ ctrlKeys ++ navKeys ++ wsKeys

appKeys = [ ("M-<Space>", safeSpawn launcher [])
          , ("M-<Return>", safeSpawn term [])
          , ("M-S-<Return>", safeSpawn altTerm [])
          , ("M-\\", safeSpawn browser [])
          , ("M-S-\\", safeSpawn altBrowser [])
          ]

winKeys = [ ("M-<Backspace>", kill1)
          , ("M-C-<Backspace>", confirmPrompt myPromptTheme "Kill all windows?" killAll)
          , ("M-S-<Backspace>", kill)
          , ("M-/", gotoMenuConfig myWGConf)
          , ("M-S-/", bringMenuConfig myWBConf)
          ]
       ++ (zipKeys "M-y " wKeys myWorkspaces (windows . copy))
       ++ addPrfx "M-y" [ ("0", windows copyToAll)
                        , ("<Backspace>", killAllOtherCopies)
                        ]


loKeys = [ ("M-v", dmenuRun "Layouts" selectLayout)
         , ("M-t", sendMessage $ JumpToLayout "mStack")
         , ("M-m", cycleThroughLayouts ["monocle", "mStack"])
         , ("M-d", cycleThroughLayouts ["deck", "mStack"])
         , ("M-S-t", cycleThroughLayouts ["mTabs", "mStack"])
         , ("M1-<Return>", windows W.swapMaster)
         , ("M-=", sendMessage $ IncMasterN 1)
         , ("M--", sendMessage $ IncMasterN (-1))
         , ("M-b", sendMessage ToggleStruts)
         , ("M-r", sendMessage $ Toggle MIRROR)
         , ("M-[", sendMessage Shrink)
         , ("M-]", sendMessage Expand)
         , ("M-S-]", sendMessage MirrorShrink)
         , ("M-S-[", sendMessage MirrorExpand)
         , ("M-C-[", asks (XMonad.layoutHook . config) >>= setLayout)
         , ("M-C-]", asks (XMonad.layoutHook . config) >>= setLayout)
         , ("M-S-=", sequence_ [ withFocused $ windows . W.sink, sendMessage $ Toggle NBFULL ])
         ]
      ++ (zipKeys' ["M-C-", "M-w "] (hjklKeys ++ arrowKeys) (cycle dir2DKeys) $ repeat (sendMessage . pullGroup))
      ++ addPrfx "M-w" [ ("u", withFocused $ sendMessage . UnMerge)
                       , ("m", withFocused $ sendMessage . MergeAll)
                       , ("<Tab>", toSubl NextLayout)
                       , ("f", withFocused toggleFloat)
                       , ("S-f", sinkAll)
                       ]

wsKeys = [ ("M-`", toggleWS)
         , ("M-n", moveTo Next HiddenWS)
         , ("M-S-n", moveTo Prev HiddenWS)
         ]
      ++ addPrfx "M-'" [ ("'", selectWorkspace myPromptTheme)
                       , ("M-'", selectWorkspace myPromptTheme)
                       , ("n", appendWorkspacePrompt myPromptTheme)
                       , ("a", appendWorkspacePrompt myPromptTheme)
                       , ("<Backspace>", removeEmptyWorkspace)
                       , ("r", renameWorkspace myPromptTheme)
                       , ("m", withWorkspace myPromptTheme (windows . W.shift))
                       ]
      ++ (zipKeys' ["M-", "M-S-"] wKeys [0..] $ withNthWorkspace <$> [W.greedyView, W.shift])

navKeys = [ ("M-S-f", switchLayer)      -- navigation2D
          , ("M-i", bindOn LD $ altAct1 ["tabs", "mTabs", "deck"] (windows W.focusDown) (onGroup W.focusDown') )
          , ("M-u", bindOn LD $ altAct1 ["tabs", "mTabs", "deck"] (windows W.focusUp) (onGroup W.focusUp') )
          , ("M-S-i", windows W.swapDown)
          , ("M-S-u", windows W.swapUp)
          , ("M-g g", windows W.focusMaster)
          , ("M-g M-g", windows W.focusMaster)
          , ("M-f", selectWindow' >>= (flip whenJust (windows . W.focusWindow)))
          ]
       ++ (zipKeys' ["M-", "M-S-"] (hjklKeys ++ arrowKeys) (cycle dir2DKeys) $ (\f -> flip f True) <$> [windowGo, windowSwap])
       ++ (zipKeys' ["M-", "M-S-", "M-C-"] [",", "."] [L, R] $ (\f -> flip f True) <$> [screenGo, windowToScreen, screenSwap])
       ++ (zipKeys' ["M-g ", "M-g S-"] (show <$> [0..9]) [0..9] [focusNth, swapNth])

ctrlKeys = [ ("M-S-c", myCommands >>= dmenuRun "XMonad")
           ]
          ++ addPrfx "M-x" [ ("c", myCommands >>= dmenuRun "XMonad")
                         , ("q", confirmPrompt myPromptTheme "Quit XMonad?" . io $ exitWith ExitSuccess)
                         , ("r", spawn "xmonad --restart")
                         , ("S-r", spawn "xmonad --recompile && xmonad --restart")
                         ]



myExtraMouseKeys :: [((ButtonMask, Button), Window -> X ())]
myExtraMouseKeys = [ ((myModMask, 4), (\_ -> moveTo Prev HiddenWS))
                   , ((myModMask, 5), (\_ -> moveTo Next HiddenWS))
                   ]








-- extra functions


altAct :: [[String]] -> [X ()] -> X () -> [(String, X ())]
altAct xss acts defact = (concat $ (\(xs, a) -> zip xs $ repeat a) <$> zip xss acts ) ++ ("", defact) : []

altAct1 :: [String] -> X () -> X () -> [(String, X ())]
altAct1 xs act defact = altAct [xs] [act] defact


-- wsSel :: [(String, X ())]
-- wsSel = [ (fst s, withNthWorkspace W.greedyView $ snd s) | s <- zip myWorkspaces [0..] ]


selectLayout :: [(String, X ())]
selectLayout = ("reset", asks (XMonad.layoutHook . config) >>= setLayout)
             : [ (s, sendMessage $ JumpToLayout s) | s <- layoutList ]

-- helpers
zipKeys :: String -> [String] -> [a] -> (a -> X ()) -> [(String, X ())]
zipKeys m ks as f = zipWith (\k a -> (m ++ k, f a)) ks as

zipKeys' :: [String] -> [String] -> [a] -> [a -> X ()] -> [(String, X ())]
zipKeys' ms ks as fs = concat $ (\(m, f) -> zipKeys m ks as f) <$> zip ms fs

addPrfx :: String -> [(String, X ())] -> [(String, X ())]
addPrfx prfx = map (\(s, x) -> (prfx ++ " " ++ s, x))

hjklKeys = ["h", "j", "k", "l"]
arrowKeys = ["<L>", "<D>", "<U>", "<R>"]
dir2DKeys = [L, D, U, R]
wKeys = show <$> [1..9] -- ++ [0]

-- misc

selectWindow' = selectWindowColors (titleFG t) (titleBG t)
    where t = def :: BarTheme



myWBConf :: WindowBringerConfig
myWBConf = def { menuArgs = ["-i", "-l", "20", "-p", "Bring Window"] }

myWGConf :: WindowBringerConfig
myWGConf = def { menuArgs = ["-i", "-l", "20", "-p", "GoTo Window"] }
