module EXMonad.Config.Layouts ( myLayoutHook
                              , myNav2DConf
                              , layoutList
                              ) where

import EXMonad.Config.Workspaces ( altLayoutWS )
import EXMonad.Config.Theme ( myTabTheme )


import XMonad hiding ( (|||) )

import XMonad.Actions.Navigation2D

import XMonad.Hooks.ManageDocks

import XMonad.Layout.Accordion
import XMonad.Layout.Circle
import XMonad.Layout.StackTile          -- resizable dishes
import XMonad.Layout.Fullscreen
import XMonad.Layout.Column
import XMonad.Layout.Grid
import XMonad.Layout.IndependentScreens (countScreens)  -- for multiple xmobars
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.LayoutBuilder      -- for building my own layouts
import XMonad.Layout.LayoutModifier (ModifiedLayout)
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.OneBig
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Renamed            -- rename layouts
import XMonad.Layout.ResizableTile
import XMonad.Layout.Simplest
import XMonad.Layout.SubLayouts
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.WindowNavigation   -- necessary for sublayouts


myNav2DConf :: Navigation2DConfig
myNav2DConf = def { defaultTiledNavigation = hybridOf sideNavigation centerNavigation
                  , floatNavigation        = hybridOf lineNavigation centerNavigation
                  , screenNavigation       = lineNavigation
                  , layoutNavigation       = [ ("Full", centerNavigation)
                                             , ("monocle", centerNavigation)
                                             , ("dishes", hybridOf sideNavigation centerNavigation)
                                             ]
                  , unmappedWindowRect     = [ ("Full", singleWindowRect)
                                             , ("monocle", singleWindowRect)
                                             ]
                  }


myLayoutHook = mkToggle (single NBFULL) -- fullscreen
             . onWorkspaces altLayoutWS altLayout
             $ mStack ||| deck ||| mTabs ||| sharedLayouts
                 where
                     altLayout     = mStack' ||| deck' ||| mTabs' ||| sharedLayouts
                     sharedLayouts = monocle ||| grid ||| threeColumn ||| oneColumn ||| tabs ||| dishes ||| accordion ||| oneBig ||| circle

layoutList = [ "mStack"
             , "monocle"
             , "deck"
             , "grid"
             , "threeColumn"
             , "oneColumn"
             , "tabs"
             , "dishes"
             , "accordion"
             , "oneBig"
             , "circle"
             , "mTabs"
             ]


named :: LayoutClass l a => String -> l a -> ModifiedLayout Rename l a
named n = renamed [(XMonad.Layout.Renamed.Replace n)]

-- each of these neads to include avoidStruts since we want fullscreen to cover the bar
-- also, we need to put MIRROR after avoidStruts due to function composition order
mStack =
    named "mStack"
  . avoidStruts
  . mkToggle1 MIRROR
  . configurableNavigation noNavigateBorders
  . addTabs shrinkText myTabTheme
  . subLayout [] (Simplest ||| Accordion)
  $ ResizableTall 1 (3/100) (1/2) []

mStack' =
    named "mStack"
  . avoidStruts
  . mkToggle1 MIRROR
  . configurableNavigation noNavigateBorders
  . addTabs shrinkText myTabTheme
  . subLayout [] (Simplest ||| Accordion)
  $ ResizableTall 1 (3/100) (2/3) []

threeColumn =
    named "threeColumn"
  . avoidStruts
  . configurableNavigation noNavigateBorders
  . addTabs shrinkText myTabTheme
  . subLayout [] (Simplest ||| Accordion)
  $ ThreeColMid 1 (3/100) (1/2)

oneBig =
    named "oneBig"
  . avoidStruts
  . mkToggle1 MIRROR
  . configurableNavigation noNavigateBorders
  . addTabs shrinkText myTabTheme
  . subLayout [] (Simplest ||| Accordion)
  $ OneBig (2/3) (2/3)

tabs      = named "tabs"      . avoidStruts . addTabs shrinkText myTabTheme $ Simplest
oneColumn = named "oneColumn" . avoidStruts . mkToggle1 MIRROR              $ Column 1.1
dishes    = named "dishes"    . avoidStruts . mkToggle1 MIRROR              $ StackTile 2 (3/100) (5/6)
accordion = named "accordion" . avoidStruts . mkToggle1 MIRROR              $ Accordion
grid      = named "grid"      . avoidStruts . mkToggle1 MIRROR              $ Grid
circle    = named "circle"    . avoidStruts $ Circle
monocle   = named "monocle"   . avoidStruts $ Full

mTabs =
    named "mTabs"
  . avoidStruts
  . layoutN 1 (relBox 0 0 (1/2) 1) (Just $ relBox 0 0 1 1) Full
  $ layoutAll (relBox (1/2) 0 1 1) (tabbed shrinkText myTabTheme)

mTabs' =
    named "mTabs"
  . avoidStruts
  . layoutN 1 (relBox 0 0 (2/3) 1) (Just $ relBox 0 0 1 1) Full
  $ layoutAll (relBox (2/3) 0 1 1) (tabbed shrinkText myTabTheme)

deck =
    named "deck"
  . avoidStruts
  . layoutN 1 (relBox 0 0 (1/2) 1) (Just $ relBox 0 0 1 1) Full
  $ layoutAll (relBox (1/2) 0 1 1) Full

deck' =
    named "deck"
  . avoidStruts
  . layoutN 1 (relBox 0 0 (2/3) 1) (Just $ relBox 0 0 1 1) Full
  $ layoutAll (relBox (2/3) 0 1 1) Full

