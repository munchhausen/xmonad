module EXMonad.Config.Commands ( myCommands ) where

import EXMonad.Config.Extras

import System.Exit

import XMonad
import qualified XMonad.StackSet as W
import XMonad.Actions.Commands
import XMonad.Actions.WithAll


myCommands :: X [(String, X ())]
myCommands = (++ (extraCMDs ++ screenCommands)) <$> workspaceCommands
        where
            extraCMDs =
                [ ("kill"         , kill)
                , ("killall"      , killAll)
                , ("master-focus" , windows W.focusMaster)
                , ("master-swap"  , windows W.swapMaster)
                , ("master-incr"  , sendMessage $ IncMasterN 1)
                , ("master-decr"  , sendMessage $ IncMasterN (-1))
                , ("sink"         , withFocused $ windows . W.sink)
                , ("sinkall"      , sinkAll)
                , ("float-toggle" , withFocused toggleFloat)
                , ("shrink"       , sendMessage Shrink)
                , ("expand"       , sendMessage Expand)
                , ("quit"         , io $ exitWith ExitSuccess)
                , ("restart"      , spawn "xmonad --restart")
                , ("reset-layout" , asks (XMonad.layoutHook . config) >>= setLayout)
                ]
