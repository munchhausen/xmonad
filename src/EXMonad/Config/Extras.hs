module EXMonad.Config.Extras ( toggleFloat ) where

import qualified Data.Map as M

import XMonad
import qualified XMonad.StackSet as W

toggleFloat :: Window -> X ()
toggleFloat w = windows (\s -> if M.member w (W.floating s)
                                  then W.sink w s
                                  else W.float w (W.RationalRect (1/3) (1/4) (1/2) (4/5)) s )
