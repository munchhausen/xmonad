module EXMonad (eXMonadMain) where

import Control.Monad ( forM )  -- (liftM, forM, forM_)
-- import Data.List
-- import qualified Data.Map as M
-- import Data.Maybe
-- import System.Exit
-- import System.IO

import Theme ( WMTheme (..), WinTheme (..) )
import EXMonad.Config.Events
import EXMonad.Config.Keys
import EXMonad.Config.Layouts
import EXMonad.Config.Log
import EXMonad.Config.StartUp
import EXMonad.Config.Workspaces

import XMonad

import XMonad.Actions.Navigation2D
import XMonad.Actions.DynamicProjects

import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.UrgencyHook (withUrgencyHook, NoUrgencyHook(..))

import XMonad.Layout.IndependentScreens (countScreens)  -- for multiple xmobars

import XMonad.Util.Run  ( spawnPipe )
import XMonad.Util.EZConfig


-- import EXMonad.Actions.ConditionalKeys
-- import EXMonad.Actions.SelectWindow      -- easymotion!


-- -- Actions
-- import XMonad.Actions.Commands          -- for xmonadctl
-- import XMonad.Actions.CopyWindow
-- import XMonad.Actions.CycleSelectedLayouts
-- import XMonad.Actions.CycleWS
-- import XMonad.Actions.DynamicWorkspaces -- withNthWorkspace
-- import XMonad.Actions.FocusNth
-- import XMonad.Actions.SpawnOn
-- import XMonad.Actions.WindowBringer
-- import XMonad.Actions.WithAll
-- -- Hooks
-- import XMonad.Hooks.DynamicLog          -- for xmobar
-- import XMonad.Hooks.InsertPosition      -- where to place newly spawned windows
-- import XMonad.Hooks.ManageDocks         -- to avoid xmobar
-- import XMonad.Hooks.ManageHelpers
-- import XMonad.Hooks.ServerMode          -- xmonadctl
-- import XMonad.Hooks.SetWMName           -- for java programs

-- -- Layouts
-- import XMonad.Layout.Accordion
-- import XMonad.Layout.Circle
-- import XMonad.Layout.Dishes
-- import XMonad.Layout.StackTile          -- resizable dishes
-- import XMonad.Layout.Fullscreen
-- import XMonad.Layout.Column
-- import XMonad.Layout.Grid
-- import XMonad.Layout.LayoutCombinators
-- import XMonad.Layout.LayoutBuilder      -- for building my own layouts
-- import XMonad.Layout.LayoutModifier (ModifiedLayout)
-- import XMonad.Layout.MultiToggle
-- import XMonad.Layout.MultiToggle.Instances
-- import XMonad.Layout.OneBig
-- import XMonad.Layout.PerWorkspace
-- import XMonad.Layout.Renamed            -- rename layouts
-- import XMonad.Layout.ResizableTile
-- import XMonad.Layout.Simplest
-- import XMonad.Layout.SubLayouts
-- import XMonad.Layout.Tabbed
-- import XMonad.Layout.ThreeColumns
-- import XMonad.Layout.WindowNavigation   -- necessary for sublayouts

-- -- Utils
-- import XMonad.Util.Cursor
-- import qualified XMonad.Util.Dmenu as D
-- import XMonad.Util.EZConfig
-- import XMonad.Util.SpawnOnce
-- import XMonad.Util.Ungrab               -- for dmenu prompts

-- import XMonad.Prompt
-- import XMonad.Prompt.XMonad
-- import XMonad.Prompt.ConfirmPrompt



eXMonadMain :: IO ()
eXMonadMain = do
    -- xmproc <- spawnPipe myStatusBar
    nScreens <- countScreens
    xmproc <- forM [0..nScreens - 1] (spawnPipe . statusOnScreen)
    xmonad . withUrgencyHook NoUrgencyHook
           . dynamicProjects projects
           . withNavigation2DConfig myNav2DConf
           . ewmh
           $ myConfig xmproc

myConfig x = def { keys = flip mkKeymap myKeys
                 , terminal = "term"
                 , modMask = myModMask
                 , clickJustFocuses = False
                 , focusFollowsMouse = False
                 , normalBorderColor = winBorder $ inactiveWin t
                 , focusedBorderColor = winBorder $ activeWin t
                 , borderWidth = 2
                 , workspaces = myWorkspaces
                 , layoutHook = myLayoutHook
                 , logHook = myLogHook x
                 , manageHook = myManageHook
                 , handleEventHook = myHandleEventHook
                 , startupHook = myStartupHook
                 }
                 `additionalMouseBindings`
                 myExtraMouseKeys
                     where t = def :: WMTheme

-- myStatusBar = "xmobar -x 0 ~/.xmonad/xmobar/xmobar.hs"
-- TODO: have this check to see if there's an alternative xmobar config for alt screens
-- Also, look into XMonad.Hooks.DynamicBars
statusOnScreen :: Int -> String
statusOnScreen i = "xmobar -x " ++ show i
