module EXmobar (eXmobarMain) where

import Xmobar

import EXmobar.Config

-- http://projects.haskell.org/xmobar/
-- install xmobar with these flags: --flags="with_alsa" --flags="with_mpd" --flags="with_xft"  OR --flags="all_extensions"
-- you can find weather location codes here: http://weather.noaa.gov/index.html

eXmobarMain :: IO ()
eXmobarMain = myConfig >>= xmobar


-- config :: Config
-- config = defaultConfig
    -- { font = "xft:monospace:size=9:antialias=true:hinting=true"
    -- , additionalFonts = [ "xft:FontAwesome:size=9", "xft:Noto Color Emoji:size=9" ]
    -- , bgColor = "#22212c"
    -- , fgColor = "#f8f8f2"
    -- , position = Top
       -- -- , position = TopW L 95
       -- -- , position = TopSize L 95 5
    -- , lowerOnStart = True
    -- , hideOnStart = False
    -- , allDesktops = True
    -- , persistent = True
    -- , border = NoBorder
    -- , borderWidth = 0
    -- , iconRoot = ".xmonad/icons"
    -- , commands =
        -- [ Run $ Com ".xmonad/xmobar/scripts/menu.sh" [] "menu" 0
        -- , Run $ Com ".xmonad/xmobar/scripts/sep.sh" [] "sep" 0
        -- , Run $ Com ".xmonad/xmobar/scripts/volume.sh" [] "vol" 1
        -- , Run $ Com ".xmonad/xmobar/scripts/weather.sh" [] "weather" 1000
        -- , Run $ Com ".xmonad/xmobar/scripts/mail.sh" [] "mail" 100
        -- , Run $ Com ".xmonad/xmobar/scripts/tray.sh" ["stalonetray"] "traypadding" 30
        -- , Run $ Date "<action=`term -e ikhal` button=1><icon=calendar/%d.xpm/></action> %I:%M%P" "date" 10
        -- , Run $ MultiCpu
            -- [ "--template" , "<action=`term -e htop` button=1><ipat></action> <total>%"
            -- , "--Low"      , "25"
            -- , "--High"     , "75"
            -- , "--low"      , "#8aff80"
            -- , "--normal"   , "#ffff80"
            -- , "--high"     , "#ff5555"
            -- , "--"
            -- , "--load-icon-pattern" , "<icon=cpu/cpu_%%.xpm/>"
            -- ] 20
        -- , Run $ Memory
            -- [ "--template" , "<action=`term -e htop` button=1><usedipat></action> <usedratio>%"
            -- , "--Low"      , "25"
            -- , "--High"     , "75"
            -- , "--low"      , "#8aff80"
            -- , "--normal"   , "#ffff80"
            -- , "--high"     , "#ff5555"
            -- , "--"
            -- , "--used-icon-pattern" , "<icon=ram/ram_%%.xpm/>"
            -- ] 20
        -- , Run $ Battery
            -- [ "--template" , "<action=`xfce4-power-manager -c` button=1><leftipat></action> <acstatus>"
            -- , "--Low"      , "20"        -- units: %
            -- , "--High"     , "80"        -- units: %
            -- , "--low"      , "#ff5555"
            -- , "--normal"   , "#ffff80"
            -- , "--high"     , "#8aff80"

            -- , "--"
            -- , "--on-icon-pattern"   , "<icon=battery/on/battery_on_%%.xpm/>"
            -- , "--off-icon-pattern"  , "<icon=battery/off/battery_off_%%.xpm/>"
            -- , "--idle-icon-pattern" , "<icon=battery/idle/battery_idle_%%.xpm/>"
            -- , "-o" , "<left>% (<timeleft>)"
            -- , "-O" , "<fc=#8aff80>C</fc>"
            -- , "-i" , "<fc=#8aff80>I</fc>"
            -- ] 50
        -- , Run $ DynNetwork
            -- [ "--template"  , "<tx><action=`nm-connection-editor` button=1><txipat><rxipat></action><rx>"
            -- , "--Low"       , "100000"   -- units: B/s
            -- , "--High"      , "1000000"  -- units: B/s
            -- , "--low"       , "#8aff80"
            -- , "--normal"    , "#ffff80"
            -- , "--high"      , "#ff5555"
            -- , "--maxtwidth" , "0"
            -- , "--"
            -- , "--rx-icon-pattern" , "<icon=network/rx/network_rx_%%.xpm/>"
            -- , "--tx-icon-pattern" , "<icon=network/tx/network_tx_%%.xpm/>"
            -- ] 20
        -- , Run $ Alsa "default" "Master"
            -- [ "--template" , "<volumebar><volume>%<status>"
            -- -- , "--highv"    , "80"
            -- -- , "--lowv"     , "20"
            -- ]
        -- , Run $ UnsafeStdinReader
        -- ]
    -- , sepChar = "%"
    -- , alignSep = "}{"
    -- , template = " %menu% %UnsafeStdinReader% }{ %sep% %multicpu% %sep% %memory% %sep% %dynnetwork% %sep% %weather% %sep% %mail% %sep% %alsa:default:Master% %sep% %battery% %sep% %date% %sep% %traypadding%"
    -- }

