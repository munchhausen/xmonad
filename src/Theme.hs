module Theme ( BarTheme (..)
             , WMTheme (..)
             , WSTheme (..)
             , WinTheme (..)
             , PromptTheme (..)
             ) where


import Data.Default



data WSTheme = WSTheme
    { wsFG :: String
    , wsBG :: String
    , wsWrap :: Bool    -- ^ whether or not WS has windows
             -> Bool    -- ^ whether or not WS has copies
             -> String  -- ^ what to actually wrap
             -> String
    }


data BarTheme = BarTheme
    { barFont :: String
    , barExtraFonts :: [String]
    , barFG :: String
    , barBG :: String
    , lnhCol :: (String, String, String)
    , titleFG :: String
    , titleBG :: String
    , currentWS :: WSTheme
    , visibleWS :: WSTheme
    , hiddenWS :: WSTheme
    , urgentWS :: WSTheme
    }



instance Default BarTheme where
    def = BarTheme
        { barFont = font
        , barExtraFonts = ["xft:symbola:size=10:antialias=true:hinting=true"]
        , barFG = fg
        , barBG = bg
        , lnhCol = (green, yellow, red)
        , titleFG = bg
        , titleBG = yellow
        , currentWS = WSTheme
            { wsFG = bg
            , wsBG = yellow
            , wsWrap = myWrap
            }
        , visibleWS = WSTheme
            { wsFG = bg
            , wsBG = orange
            , wsWrap = myWrap
            }
        , hiddenWS = WSTheme
            { wsFG = fg
            , wsBG = bg
            , wsWrap = myWrap
            }
        , urgentWS = WSTheme
            { wsFG = red
            , wsBG = bg
            , wsWrap = (\b1 b2 str -> wrap "!" "!" str)
            }
        }



data PromptTheme = PromptTheme
    { pFont :: String
    , pFG :: String
    , pBG :: String
    , pHLFG :: String
    , pHLBG :: String
    , pBorder :: String
    }

instance Default PromptTheme where
    def = PromptTheme
        { pFont = font
        , pFG = bg
        , pBG = yellow
        , pHLFG = yellow
        , pHLBG = bg
        , pBorder = bg
        }



data WinTheme = WinTheme
    { winFG :: String
    , winBG :: String
    , winBorder :: String
    }

data WMTheme = WMTheme
    { wmFont :: String
    , activeWin :: WinTheme
    , inactiveWin :: WinTheme
    , urgentWin :: WinTheme
    }

instance Default WMTheme where
    def = WMTheme
        { wmFont = font
        , activeWin = WinTheme
            { winFG = fg
            , winBG = bg
            , winBorder = pink
            }
        , inactiveWin = WinTheme
            { winFG = comment
            , winBG = bgdarker
            , winBorder = bgdarker
            }
        , urgentWin = WinTheme
            { winFG = red
            , winBG = bgdarker
            , winBorder = red
            }
        }





-- helpers
wrap :: String -> String -> String -> String
wrap left right middle = left ++ middle ++ right

wrapL :: String -> String -> String
wrapL s = wrap s ""

wrapR :: String -> String -> String
wrapR s = wrap "" s


wrapIt :: String -> String -> Bool -> Bool -> String -> String
wrapIt strL strR windows copies = leftWrap . rightWrap
    where
        leftWrap = if windows then wrapL strL else wrapL " "
        rightWrap = if copies then wrapR strR else wrapR " "

myWrap :: Bool -> Bool -> String -> String
myWrap = wrapIt "*" "^"




font = "xft:monospace:size=9:antialias=true:hinting=true"
bg = "#22212C"
fg = "#f8f8f2"
cyan = "#80ffea"
green = "#8aff80"
orange = "#ffca80"
pink = "#ff80bf"
purple = "#9580ff"
blue = purple
red = "#ff5555"
yellow = "#ffff80"
comment = "#7970a9"
selection = "#454158"
bgdarker = "#0b0b0f"
